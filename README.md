# N100 Vibration sensor
Application uses the accelerometer to detect movement or significant vibrations.  

**Version: 1.006<br/>**
**Release Date: 2023/09/19**

## Key Features  

1. Reports vibration or movement for continuous 2 second of motion.
2. Network check minimum 4 times a day.
5. Charging, discharging, and battery status (every 5%) reports.
6. 1.5+ yrs Battery Life.
7. Network status indicator
8. Battery status indicator
9. Sends initial location message immediately after reset.
 
# User Interface 

The N100 user interface is comprised of two LEDs and two buttons.

**Button Descriptions**

The following table summarizes the user interface for the application.  See the sections
below for more detailed information regarding each button action.  

There are two user actions battery and network status that can be accessed
using either of the buttons.
 
| Action                               |      Button        | Description                                  |
|:-------------------------------------|:------------------:|:---------------------------------------------|
| single-click                         | any button or both | Battery status (see Battery Indicator below) |
| double-click                         | any button or both | Network status (see Coverage Indicator below)|
| push and hold longer than 10 seconds | any button or both | Resets the device.                           |


**LED Descriptions** 

The following table indicates the meaning of the two bi-color LEDs on the device.  The LED's are
positioned:

* **LED #1** - Upper LED, furthest away from charger pads
* **LED #2** - Lower LED, closest to charger pads

| Indication                 | LEDS     | Description |
|:---------------------------| :-------:|:----------------------------|
| One second orange blink    | LED #2   | Battery charging            |
| Continuous green           | LED #2   | Battery fully charged       |
| Red blink 5   times        | LED #1   | Movement detected           |
| Green blink three times    | both     | Device reset                |

See status indicators below for additional LED signalling when requesting battery and network status.
 
### Battery Status Indicator 

Quickly pressing Button #2 will indicate the percent charge of the battery in 20% increments.

LED #2 will blink as follows to indicate percent charge.

| LED #2   | % Charge    | Description                                     |
|----------|-------------|-------------------------------------------------|
|  1 red   |   < 10%     | Battery level is critical,  charge immediately. |
|  1 green |  15% - 30%  | Battery low  less than 30% remaining.           |
|  2 green |  30% - 50%  | Battery is about 40% charged.                    |
|  3 green |  50% - 70%  | Battery is about 60% charged.                    |
|  4 green |  70% - 90%  | Battery  is about 80% charged.                   |
|  5 green |  > 90%      | Battery is fully charged.                       |

### Network Status Indicator 

Quickly pressing Button #2 twice will indicate the status of LoRaWAN network coverage.  

LED #1 will blink as follows to indicate coverage information

| LED #1   | Signal Strength (dBm) | Description                                |
|----------|-----------------------|--------------------------------------------|
|  4 green |  -64  to -30          | Very strong signal strength                |
|  3 green |  -89  to  -65         | Good signal strength                       |
|  2 green |  -109  to -90         | Low signal strength                        |
|  1 green |  -120 to -110         | Very low signal strength                   |
|  1 red   |                       | Network unavailable (out of range)         |
|  2 red   |                       | LoRaWAN telemetry disabled.                |

## Building and Publishing

This section provide the steps for compiling and publishing the vibration-sensor app.

### Compiling the App
To compile the app use the compile executable with the following command line:

    compile <version: eg. v1.234> [SYSLOG]

The command line requires a version tag and optional flags in the specified order:

* `SYSLOG` - enables system logs by default.
versus the normal release.

### Compiling and Publishing.  
To build and publish both release and test apps simultaneously use the Compile_Publish.bat.  This ensures version and build configuration
are handled consistently for the release and test targets.

    compile_publish <version: eg. v1.234 >

---
*Copyright 2021, Codepoint Technologies, Inc.* 
*All Rights Reserved*
  
