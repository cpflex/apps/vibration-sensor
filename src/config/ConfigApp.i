/**
*  Name:  ConfigApp.i
*
*  This module is the proprietary property of Codepoint Technologies, Inc.
*  Copyright (C) 2021 Codepoint Technologies, Inc.
*  All Rights Reserved
*
* Description:
*  Configuration constants for network link Checks.
**/

#include "Controller.i"
#include "motion.i"

const ControllerState: CONTROLLER_DEFAULT_STATE		= STATE_STARTUP;
const MotionResolution: CONTROLLER_IDLE_RESOLUTION	= MRES_LOW_POWER;
const MotionSampling: CONTROLLER_IDLE_RATE			= MRATE_1HZ;
const CONTROLLER_IDLE_THRESHOLD     = 50;

const MotionResolution: CONTROLLER_MOTION_RESOLUTION	= MRES_LOW_POWER;
const MotionSampling: CONTROLLER_MOTION_RATE		= MRATE_10HZ;
const CONTROLLER_MOTION_THRESHOLD   = 50;

const MOTION_SAMPLING_PERIOD		= 1; //Motion sampling period in seconds must be less than intervals.

