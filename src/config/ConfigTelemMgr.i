/**
*  Name:  ConfigSimpleTracking.i
*
*  This module is the proprietary property of Codepoint Technologies, Inc.
*  Copyright (C) 2021 Codepoint Technologies, Inc.
*  All Rights Reserved
*
* Description:
*  Configuration constants for network link Checks.
**/

#include "ui.i"

// Telemetry Status Indications.
const  INDICATE_LED_NORMAL   = LED1;
const  INDICATE_LED_CRITICAL = LED2;
const  INDICATE_DISABLED_ON   = 500;
const  INDICATE_DISABLED_OFF  = 250;
const  INDICATE_UNAVAILABLE_ON   = 750;
const  INDICATE_UNAVAILABLE_OFF  = 250;
const  INDICATE_RSS_ON      = 500;		
const  INDICATE_RSS_OFF     = 250;

const  DEFAULT_POLLING = 360;  // Default polling interval is every 6 hours.
