/**
 *  Name:  app.i
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2021 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */
 
#include "ui.i"
#include "timer.i"
#include "log.i"
#include "telemetry.i"
#include "NvmRecTools.i"
#include "OcmNidDefinitions.i"
 

/**
* @brief  Application Defined callback handler that indicates entering power savings.
* @remarks Required implementation to catch enter power saving events.
*/
forward app_EnterPowerSaving(pctCharged);

/**
* @brief  Application Defined callback handler that indicates entering power savings.
* @remarks Required implementation to catch exit power saving events.
*/
forward app_ExitPowerSaving(pctCharged);