/*******************************************************************************
*                !!! OCM CODE GENERATED FILE -- DO NOT EDIT !!!
*
* Vibration Sensor Application CP-Flex Application Protocol
*  nid:        VibrationSensorApi
*  uuid:       b71e954a-1a92-473e-b667-62ee903edbb7
*  ver:        1.0.0.0
*  date:       2021-11-13T19:28:07.698Z
*  product: Nali N100
* 
*  Copyright 2021 - Codepoint Technologies, Inc. All Rights Reserved.
* 
*******************************************************************************/
const OcmNidDefinitions: {
	NID_SensorStateIdle = 1,
	NID_ArchiveErase = 2,
	NID_SensorBatteryDischarging = 3,
	NID_ArchiveSyslog = 4,
	NID_SensorBatteryCharging = 5,
	NID_ArchiveSyslogInfo = 6,
	NID_ArchiveSyslogAlert = 7,
	NID_AdminReset = 8,
	NID_SensorState = 9,
	NID_SensorBattery = 10,
	NID_SensorTemperature = 11,
	NID_SensorBatteryCritical = 12,
	NID_ArchiveSyslogAll = 13,
	NID_SensorStateActivated = 14,
	NID_ArchiveSyslogDisable = 15,
	NID_SensorBatteryCharged = 16,
	NID_ArchiveSyslogDetail = 17,
	NID_SensorBatterylevel = 18
};
