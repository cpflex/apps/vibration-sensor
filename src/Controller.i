/**
 *  Name:  Controller.i
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2021 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */

 #include "telemetry.i"

 /**
 * @brief Defines the states of the mode controller.
 */
 const ControllerState: {
	STATE_STARTUP=0,			//Device is starting up
	STATE_IDLE = 1,				//Controller is in an idle state waiting for a motion event.
	STATE_ENTER_MOTION,			//Controller is determining is device is in continuous motion.
	STATE_EXIT_MOTION,			//Controller is determining if no motion is continous for some period of time.
	STATE_DISABLED,				//Controller is in a disabled state typically when power saving is enabled.
	STATE_RECOVERY				//Controller is in a recovery mode periodically reporting location.	
 };
 

/**
* @brief Initializes the controller 
*/
forward ControllerInit( );

/**
* @brief Enables / Disables recovery mode.
*/
forward ControllerRecoveryMode( bool: bEnable);


/**
* @brief Returns true if received message can be
*  processed by the Controller.
*/
forward bool: ControllerIsSupportedMessage(tmsg, id);

/**
* @brief Processes received command messages relating to the controller 
*  and tracking.
*/
forward ResultCode: ControllerProcessReceived(
	Sequence: seqOut, MsgType: tmsg, id, Message: msg);

/**
* @brief Notifies controller when power savings changes 
*/
forward ControllerPowerSavingUpdate( bool: bEnable);

/**
* @brief Notifies the controller with the telemtry network status changes.
*/
forward ControllerTelemetryStatusUpdate( TelemState: stateTelem, datarate, packetsize);