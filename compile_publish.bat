@ECHO OFF
if [%1]==[] goto usage


echo ** Compiling /  Publishing Release Version Application with SysLog Enabled 
call compile %1_syslog  SYSLOG
call cphub_push %1_syslog

echo ** Compiling /  Publishing Release Version Application
call compile %1
call cphub_push %1

goto :eof
:usage
@echo Usage: %0 ^<tag (e.g. v1.0.2.3)^> 
exit /B 1
