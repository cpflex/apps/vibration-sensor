# Vibration Sensor Application Release Notes


### V1.006 - 230919
1. Firmware Reference v2.3.0.0
   * Incorporates all stabilization improvements.  This is a nearly final release candidate for the firmware.
2. Flex Platform: v1.4.0.2
3. Standard EU band ADR enabled.

### 1.005, 230502
1. Firmware reference v2.1.1.1a
   * Changed I2C memory bus from 100Khz to 400Khz.
   * Fixed accelerometer and i2c memory bugs.
   * Changed BVT LED behavior (manufacturing).
2. Platform version v1.3.1.4

### V1.004 230417
1. Updated Firmware Bundle to V2.0.15.0a
   - Reworked positioning control to ensure OPL is shutdown to conserve power.
   - Includes updates from alpha bundle v2.0.14.0a.

### V1.003 230216 ###

1. Updated firmware reference to v1.0.10.4
2. Updated Platform SDK v1.3.0.0


### V1.001 211115 ###

1. Initial Release
2. Updated firmware reference to v1.0.1.3a_dr1


---
*Copyright 2019-2021, Codepoint Technologies,* 
*All Rights Reserved*
